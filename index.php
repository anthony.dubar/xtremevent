
<!DOCTYPE html>
<html lang="en"><!-- Basic -->
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
     <!-- Site Metas -->
    <title>Extrem Event's</title>  
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

    <![endif]-->
 <meta name='robots' content='max-image-preview:large' />
<link rel='dns-prefetch' href='//oss.maxcdn.com' />
<link rel='dns-prefetch' href='//s.w.org' />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.1.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.1.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/xtrem.re\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.8"}};
			!function(e,a,t){var n,r,o,i=a.createElement("canvas"),p=i.getContext&&i.getContext("2d");function s(e,t){var a=String.fromCharCode;p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,e),0,0);e=i.toDataURL();return p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,t),0,0),e===i.toDataURL()}function c(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(o=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},r=0;r<o.length;r++)t.supports[o[r]]=function(e){if(!p||!p.fillText)return!1;switch(p.textBaseline="top",p.font="600 32px Arial",e){case"flag":return s([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])?!1:!s([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!s([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]);case"emoji":return!s([10084,65039,8205,55357,56613],[10084,65039,8203,55357,56613])}return!1}(o[r]),t.supports.everything=t.supports.everything&&t.supports[o[r]],"flag"!==o[r]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[o[r]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(n=t.source||{}).concatemoji?c(n.concatemoji):n.wpemoji&&n.twemoji&&(c(n.twemoji),c(n.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
	<link rel='stylesheet' id='wp-block-library-css'  href='https://xtrem.re/wp-includes/css/dist/block-library/style.min.css?ver=5.8' type='text/css' media='all' />
<link rel='stylesheet' id='main-css-css'  href='https://xtrem.re/wp-content/themes/Template%20Xtrem/style.css?ver=5.8' type='text/css' media='all' />
<link rel='stylesheet' id='custom-css-css'  href='https://xtrem.re/wp-content/themes/Template%20Xtrem./css/custom.css?ver=5.8' type='text/css' media='all' />
<link rel='stylesheet' id='classic-css-css'  href='https://xtrem.re/wp-content/themes/Template%20Xtrem./css/classic.css?ver=5.8' type='text/css' media='all' />
<link rel='stylesheet' id='startup-boostrap-css-css'  href='https://xtrem.re/wp-content/themes/Template%20Xtrem./css/responsive.css?ver=5.8' type='text/css' media='all' />
<link rel='stylesheet' id='bootstrap-css-css'  href='https://xtrem.re/wp-content/themes/Template%20Xtrem/css/bootstrap.min.css?ver=5.8' type='text/css' media='all' />
<link rel='stylesheet' id='superslides-css-css'  href='https://xtrem.re/wp-content/themes/Template%20Xtrem/css/superslides.css?ver=5.8' type='text/css' media='all' />
<link rel='stylesheet' id='animate-css-css'  href='https://xtrem.re/wp-content/themes/Template%20Xtrem/css/animates.css?ver=5.8' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css-css'  href='https://xtrem.re/wp-content/themes/Template%20Xtrem/css/font-awesome.min.css?ver=5.8' type='text/css' media='all' />
<link rel='stylesheet' id='baguetteBox-css-css'  href='https://xtrem.re/wp-content/themes/Template%20Xtrem/css/baguetteBox.min.css?ver=5.8' type='text/css' media='all' />
<link rel='stylesheet' id='calendarPicker-css-css'  href='https://xtrem.re/wp-content/themes/Template%20Xtrem/css/calendarPicker.style.css?ver=5.8' type='text/css' media='all' />
<script type='text/javascript' src='https://xtrem.re/wp-includes/js/jquery/jquery.min.js?ver=3.6.0' id='jquery-core-js'></script>
<script type='text/javascript' src='https://xtrem.re/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2' id='jquery-migrate-js'></script>
<!--[if lt IE 9]>
<script type='text/javascript' src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js?ver=3.7.0' id='simple-blog-html5-js'></script>
<![endif]-->
<!--[if lt IE 9]>
<script type='text/javascript' src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js?ver=1.4.2' id='simple-blog-respondjs-js'></script>
<![endif]-->
<link rel="https://api.w.org/" href="https://xtrem.re/wp-json/" /><link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://xtrem.re/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://xtrem.re/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 5.8" />

</head>

<body data-rsssl=1>
 
 	<!-- Start header -->
	<header class="top-navbar">
		<nav class="navbar navbar-expand-lg navbar-light" id= "navbar">
			<div class="container">
				<a class="navbar-brand" href="">
					<img style="width:30%;opacity:1;" src="https://xtrem.re/wp-content/themes/Template%20Xtrem/images/logo2.png" />
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-rs-food" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbars-rs-food" id="">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item active"><a  style="background-color:rgba(9,103,15,0.8);" class="nav-link">Accueil</a></li>
						<li class="nav-item"><a class="nav-link" href="https://xtrem.re/parcours/">Parcours</a></li>
						<li class="nav-item"><a class="nav-link" href="https://xtrem.re/events/">Evenements</a></li>
						<li class="nav-item"><a class="nav-link" href="https://xtrem.re/tarifs/">Tarifs</a></li>
						<li class="nav-item"><a class="nav-link" href="https://xtrem.re/contact/">Contact</a></li>
						
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<!-- End header -->
<!-- the beginning of the page or post content -->
	<!-- Start slides -->
	<div style="height:50vh;" id="slides" class="cover-slides">
		<ul class="slides-container">
			<li >
				<img src="https://xtrem.re/wp-content/themes/Template%20Xtrem/images/image1.jpg" alt="">
				<div class="container">
					<div style="margin-top:30px;" class="row">
						<div class="col-md-12"> 
						<h2 style="color:white;"class="m-b-20"><strong>Bienvenue <br> dans notre univers</strong></h2>

								<hr style="background-color:#09670F;height:1px;width:5%;margin-right:95%;">

							<h6 style="color:white;">Prêt à repousser vos limites</h6>
							<p><a style="background-color:rgba(9,103,15,0.6);color:white;" class="btn " href="#">Réservez votre parcours</a></p></div>
					</div>
				</div>
			</li>
			<li >
				
<img src="https://xtrem.re/wp-content/themes/Template%20Xtrem/images/image2.jpg" class="img-fluid" alt="">
				<div class="container">
					<div style="margin-top:30px;" class="row">
						<div class="col-md-12"> 
							<h2 style="color:white;"class="m-b-20"><strong>Bienvenue <br> dans notre univers</strong></h2>

								<hr style="background-color:#09670F;height:1px;width:5%;margin-right:95%;">

							<h6 style="color:white;">Prêt à repousser vos limites</h6>
							<p><a style="background-color:rgba(9,103,15,0.6);color:white;" class="btn " href="#">Réservez votre parcours</a></p></div>
					</div>
				</div>
			</li>
			<li >
				
<img src="https://xtrem.re/wp-content/themes/Template%20Xtrem/images/image3.jpg" class="img-fluid" alt="">
				<div class="container">
					<div style="margin-top:30px;" class="row">
						<div class="col-md-12"> 
							<h2 style="color:white;"class="m-b-20"><strong>Bienvenue <br> dans notre univers</strong></h2>

								<hr style="background-color:#09670F;height:1px;width:5%;margin-right:95%;">

							<h6 style="color:white;">Prêt à repousser vos limites</h6>
							<p><a style="background-color:rgba(9,103,15,0.6);color:white;" class="btn " href="#">Réservez votre parcours</a></p>
						</div>
					</div>
				</div>
			</li>
		</ul>
	
	</div>
	<!-- End slides -->
	
	<!-- Start About -->
	<div style="margin-left:10%;margin-right:10%;"class="inner-column">
		<h1 style="margin-top:30px;">Vous aimez le sport ? <span>Nous aussi!</span></h1>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque auctor suscipit feugiat. Ut at pellentesque ante, sed convallis arcu. Nullam facilisis, eros in eleifend luctus, odio ante sodales augue, eget lacinia lectus erat et sem. </p>
		<p>Sed semper orci sit amet porta placerat. Etiam quis finibus eros. Sed aliquam metus lorem, a pellentesque tellus pretium a. Nulla placerat elit in justo vestibulum, et maximus sem pulvinar.</p>
	</div>


	  <div class="row special-list" style="position: relative; height: 810px;">
		<div class="col-lg-4 col-md-6 special-grid drinks">
			<div class="gallery-single fix"id="card2">
				
					<a href="https://xtrem.re/propos/"><img class="img-fluid" src="https://xtrem.re/wp-content/themes/Template%20Xtrem/images/2.png" alt=""></a>

				<div class="hero" id="hero">
					<a href="https://xtrem.re/propos/"><h2 style="color: white;">Extrem Event's</h2></a>
					<a href="https://xtrem.re/propos/"><p style="color:#09670F;">C'est quoi ?</p></a>
					</div> 
			</div>
		</div>
		
		<div class="col-lg-4 col-md-6 special-grid drinks" style="position: absolute; left: 380px; top: 0px;">
			<div class="gallery-single fix"id="card2">
				
				<a href="https://xtrem.re/events/"><img class="img-fluid" src="https://xtrem.re/wp-content/themes/Template%20Xtrem/images/3.png" alt=""></a>

				<div class="hero" id="hero">
					<a href="https://xtrem.re/events/"><h2 style="color: white;">Nos prochains</h2></a>
					<a href="https://xtrem.re/events/"><p style="color:#09670F;">Evénements</p></a>
					</div> 
			</div>
		</div>
		
		<div class="col-lg-4 col-md-6 special-grid drinks" style="position: absolute; left: 760px; top: 0px;">
			<div class="gallery-single fix" id="card2"	>

				<a href="https://xtrem.re/parcours/"><img class="img-fluid" src="https://xtrem.re/wp-content/themes/Template%20Xtrem/images/1.png" alt="">

				<div class="hero" id="hero">
					<a href="https://xtrem.re/parcours/"><h2 style="color: white;">Découvrir</h2></a>
					<a href="https://xtrem.re/parcours/"><p style="color:#09670F;">Nos parcours</p></a>
					</div> 
			
			</div>
		</div>
	</div>
	<!-- End About -->
	

	<!-- ESSAI 2-->

	<section class="homepage-values">
		<div class="container">
			<div class="row">
				<div class="col-lg-5">
					<img class="w-100" style="margin-top:30%;width:5rem;" src="https://xtrem.re/wp-content/themes/Template%20Xtrem/images/11.png" alt="">
				</div>
				<div class="col-1"></div>
				<div class="col-lg-6">
					<div class="why-us__inner">
						<div class="why-us__inner-title">
							<h2 class="h2">
								<span class="c__red">4 raisons de choisir</span>
								<br> de réserver nos parcours
							</h2>
						</div>
	
						<div class="why-us__inner-description">
							<!-- 01 -->
							<div class="why-us__inner-description-item">
								<div class="row justify-content-center">
									<div class="col-12">
										<div class="row">
											<div class="col-2">
												<p class="text-center"><img class="w-100" src="https://xtrem.re/wp-content/themes/Template%20Xtrem/images/mountains.svg"></p>
											</div>
											<div class="col-10">
												<p>On vous propose des activités insolites tout au long de l'année.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- 02 -->
							<div class="why-us__inner-description-item">
								<div class="row justify-content-center">
									<div class="col-12">
										<div class="row">
											<div class="col-2">
												<p class="text-center"><img class="w-100" src="https://xtrem.re/wp-content/themes/Template%20Xtrem/images/calendar.svg"></p>
											</div>
											<div class="col-10">
												<p>Réservation en ligne rapide et sécurisée.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- 03 -->
							<div class="why-us__inner-description-item">
								<div class="row justify-content-center">
									<div class="col-12">
										<div class="row">
											<div class="col-2">
												<p class="text-center"><img class="w-100" src="https://xtrem.re/wp-content/themes/Template%20Xtrem/images/chronometer.svg"></p>
											</div>
											<div class="col-10">
												<p>Faites du sport dans une ambiance conviviale.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- 04 -->
							<div class="why-us__inner-description-item mgb-0">
								<div class="row justify-content-center">
									<div class="col-12">
										<div class="row">
											<div class="col-2">
												<p class="text-center"><img class="w-100" src="https://xtrem.re/wp-content/themes/Template%20Xtrem/images/support.svg"></p>
											</div>
											<div class="col-10">
												<p>On vous propose des activités insolites tout au long de l'année.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
								<a href="" style="background-color:#09670F;color:white;" class="btn ">Découvrir nos offres</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	
	
	
	

	<!-- Start Footer -->
	<footer class="footer desktop">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-2 col-sm-12">
				<div class="footer__item">
					<h4>En savoir plus</h4>
					<hr style="background-color:#09674F"class="divider">
					<ul>
                        <li><a style="color:black;" href="">A propos</a></li>
                        <li><a style="color:black;" href="">Plans du site</a></li>
                        <li><a style="color:black;" href="">F.A.Q</a></li>
                        <li><a style="color:black;" href="">Contactez-nous</a></li>
                        <li><a style="color:black;" href="">Nos Valeurs</a></li>
						<li><a style="color:black;" href="">Mentions legales</a></li>
						<li><a style="color:black;" href="">CGV</a></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-2 col-sm-12">
				<div class="footer__item">
					<h4>Suivez-nous</h4>
					<hr style="background-color:#09674F" class="divider">
					<ul>
						<li><a style="color:black;"  href="" target="_blank">Facebook</a></li>
						<li><a style="color:black;"  href="" target="_blank">Instagram</a></li>
						<li><a style="color:black;"  href="" target="_blank">Youtube</a></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-2"></div>
			<!-- Newsletter -->
			<div class="col-lg-6 col-sm-12">
				<h4>Inscrivez-vous a la newsletter</h4>
				<hr style="background-color:#09670F"class="divider">

				<p><p class="c__light mgb-10">Abonnez-vous pour suivre notre actualite. C'est promis on vous tient au jus !</p></p>

				<div class="newsletter-section index-section--newsletter-background">
					<div class="page-width">
						<div class="section-header text-center">
							
						</div>

													
							<input type="hidden" name="contact" value="newsletter">
							<div class="input-group ">
								<div class="input-group mb-3">
								<input type="text" class="form-control" placeholder="Entrez votre mail" aria-label="Recipient's username" aria-describedby="button-addon2">
									 <div class="input-group-append">
								<button class="btn " style="background-color:#09670F;color:white" type="button" id="button-addon2">S inscrire</button>
									</div>
										</div>

							
								<p class="c__green mgt-10 form-message form-message--success" tabindex="-1" data-form-status>Merci pour votre inscription</p>
						

							
								
						
					</div>
				</div>
          	 <p class="c__light mgb-10" style="font-size:1rem" >Xtremevent a ete cofinancee par l Union Europeenne et la Region Reunion
              </p><br><p> 
          </p>
			</div>
		</div>
	</div>
</footer>

	<!-- End Footer -->
	
	<a href="#" id="back-to-top" title="Back to top" style="display: none;">&uarr;</a>
  
    <!-- webpage content -->
    <script type='text/javascript' src='https://xtrem.re/wp-content/themes/Template%20Xtrem/js/jquery-3.2.1.min.js?ver=5.8' id='jquery-js-js'></script>
<script type='text/javascript' src='https://xtrem.re/wp-content/themes/Template%20Xtrem/js/popper.min.js?ver=5.8' id='popper-js-js'></script>
<script type='text/javascript' src='https://xtrem.re/wp-content/themes/Template%20Xtrem/js/bootstrap.min.js?ver=5.8' id='bootstrap-js-js'></script>
<script type='text/javascript' src='https://xtrem.re/wp-content/themes/Template%20Xtrem/js/jquery.superslides.min.js?ver=5.8' id='superslides-js-js'></script>
<script type='text/javascript' src='https://xtrem.re/wp-content/themes/Template%20Xtrem/js/images-loded.min.js?ver=5.8' id='images-loded-js-js'></script>
<script type='text/javascript' src='https://xtrem.re/wp-content/themes/Template%20Xtrem/js/isotope.min.js?ver=5.8' id='isotope-js'></script>
<script type='text/javascript' src='https://xtrem.re/wp-content/themes/Template%20Xtrem/js/baguetteBox.min.js?ver=5.8' id='baguetteBox-js-js'></script>
<script type='text/javascript' src='https://xtrem.re/wp-content/themes/Template%20Xtrem/js/form-validator.min.js?ver=5.8' id='form-validator-js-js'></script>
<script type='text/javascript' src='https://xtrem.re/wp-content/themes/Template%20Xtrem/js/contact-form-script.js?ver=5.8' id='contact-form-script-js-js'></script>
<script type='text/javascript' src='https://xtrem.re/wp-content/themes/Template%20Xtrem/js/custom.js?ver=5.8' id='custom-js-js'></script>
<script type='text/javascript' src='https://xtrem.re/wp-content/themes/Template%20Xtrem/js/CalendarPicker.js?ver=5.8' id='calendarPicker-js-js'></script>
<script type='text/javascript' src='https://xtrem.re/wp-content/themes/Template%20Xtrem/js/CalendarPicker2.js?ver=5.8' id='calendarPicker2-js-js'></script>
<script type='text/javascript' src='https://xtrem.re/wp-includes/js/wp-embed.min.js?ver=5.8' id='wp-embed-js'></script>
	<!-- ALL JS FILES -->

</body>
</html>

